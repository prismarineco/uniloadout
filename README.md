# The Universal Loadout System

The _Universal Loadout System_, a.k.a. _`UNILOADOUT`_, is a web API designed to manage Splatoon 2 player profiles across various services, such as websites or discord bots. Users would provide their data to the API, which they could then provide to other services through their Player ID, a unique identifier that acts as a key to their information in the API.

## NOTE

_`UNILOADOUT`_ is in an **unfinished** state. Much of its functionality is unimplemented, and it much of what it does have is likely ripe for abuse. The release of this repository is to gather both public feedback for features and assistance with implementation, so that when the _`UNILOADOUT`_ API goes live on [The Prismarine Company website](https://prismarine.co), it will be in a much more robust, feature-complete, and useful state.

As it stands, it is currently little more than a proof-of-concept.

## CONFIGURATION

_`UNILOADOUT`_ depends upon a `.env` file within the current working directory for its configuration. The following variables are read by the program on startup:

- `DATABASE_URL`: Self-explanatory. Should be a `mysql` database, as other RDBMS systems are not supported. Required to be set at compile time.
- `TERMINAL_RUST_LOG` / `FILE_RUST_LOG`: Sets logging configuration relative to normal `RUST_LOG` syntax. If you want to know what that syntax is, refer to [`slog-envlogger`](https://docs.rs/slog-envlogger).
- `FILE_LOG_PATH`: Tells the program where to output file logging. Defaults to `uniloadout.log` if unset.

## RUNNING THE SYSTEM

Once configured, the system is fairly straight forward to run. You're gonna want to install the `sqlx-cli` (via `cargo install`), and with the `DATABASE_URL` set in your `.env`, run `sqlx db create && sqlx migrate run`. This will generate the database and all the tables with it. Once generated, all you need to do is use `cargo run`.

## ENDPOINTS

See [the endpoint document](docs/endpoints.md).

## LICENSE

Copyright (C) 2021 DrBluefall.

The Universal Loadout System is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
