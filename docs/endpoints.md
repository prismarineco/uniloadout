# Universal Loadout System: Endpoints

## Player Endpoints

Player Object form (as described with Rust types):

```
{
    "name": String,
    "level": u16,
    "id": uuid::Uuid,
    "ranks": {
        "splat_zones": String,
        "tower_control": String,
        "rainmaker": String,
        "clam_blitz": String,
        "salmon_run": u8
    }
    "loadouts": HashSet<Loadout>
}
```

Loadout Object form:

```
{
    "wep_set": u32,
    "wep_id": u32,
    "head": {
        "gear_id": u16,
        "main": u8,
        "subs": [u8; 3]
    },
    "clothing": {
        "gear_id": u16,
        "main": u8,
        "subs": [u8; 3]
    },
    "shoes": {
        "gear_id": u16,
        "main": u8,
        "subs": [u8; 3]
    },
}
```

Ranks are expected to be, and are serialized as, a certain format. Generally, so long as they fit the following regex:

```regex
/[CBA][+-]?|S(\+\d?)?|X\d{4}/
```

They are considered well formed and will be accepted by the API.

### POST `/player/new`

Register a new player within the API. The player's `name` must be specified within the query string, e.g. `/player/new?name=YajuShinki`

### GET `/player/{player_id}`

Retrieve a player's info from the API.

### PATCH `/player/{player_id}`

Modify a player's info within the API, sending a JSON body with the fields you want to modify. Returns `204 NO CONTENT` on success.

Most fields mirror the standard structure of the Player Object, but with 2 extra additons: `"loadout_add"` and `"loadout_remove"`, a pair of `HashSet<String>`s. The strings within would be loadout.ink-style encoded loadouts. For info on how to encode loadouts in this manner, refer to [this file](https://github.com/selicia/splat2-calc/blob/693a62767f8061827053a483ecfb5f9c3a8a5e82/app/encode.js) in the loadout.ink repository.

### DELETE `/player/{player_id}`

Remove a player from the API. Returns `204 NO CONTENT` on success.
