-- -*- sql-product: mysql; eval: (flycheck-disable-checker 'sql-sqlint) -*-

CREATE TABLE loadouts (
       id BINARY(16) NOT NULL, -- Player UUID
       ld CHAR(25) NOT NULL, -- hex-encoded loadout
       INDEX (id),
       UNIQUE (id,ld),
       FOREIGN KEY (id) REFERENCES players(id)
               ON DELETE CASCADE
) CHARACTER SET 'utf8mb4' COLLATE utf8mb4_unicode_ci;
