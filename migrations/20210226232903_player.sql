-- -*- sql-product: mysql; eval: (flycheck-disable-checker 'sql-sqlint) -*-

CREATE TABLE players (
      id BINARY(16), -- Player UUID
      name VARCHAR(10) NOT NULL,
      level SMALLINT UNSIGNED NOT NULL,
      PRIMARY KEY (id)
) CHARACTER SET 'utf8mb4' COLLATE utf8mb4_unicode_ci;
