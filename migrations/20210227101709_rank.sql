-- -*- sql-product: mysql; eval: (flycheck-disable-checker 'sql-sqlint) -*-

CREATE TABLE ranks (
      id BINARY(16), -- Player UUID
      sz_rank TINYINT UNSIGNED NOT NULL,
      sz_power SMALLINT UNSIGNED,
      tc_rank TINYINT UNSIGNED NOT NULL,
      tc_power SMALLINT UNSIGNED,
      rm_rank TINYINT UNSIGNED NOT NULL,
      rm_power SMALLINT UNSIGNED,
      cb_rank TINYINT UNSIGNED NOT NULL,
      cb_power SMALLINT UNSIGNED,
      salmon_run TINYINT UNSIGNED NOT NULL,
      PRIMARY KEY (id),
      FOREIGN KEY (id)
              REFERENCES players(id)
              ON DELETE CASCADE
);
