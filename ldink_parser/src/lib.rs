#[cfg(feature = "serde-derive")]
extern crate serde;
extern crate substring;
use substring::Substring;

pub mod gear_item;
pub use gear_item::GearItem;

#[derive(Debug, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde-derive", derive(serde::Serialize, serde::Deserialize))]
pub struct Loadout {
    pub wep_set: u32,
    pub wep_id: u32,
    pub head: GearItem,
    pub clothes: GearItem,
    pub shoes: GearItem,
}

impl Loadout {
    /// Encode a loadout into a hexidecimal string.
    pub fn encode(&self) -> String {
        let mut encoded = String::new();

        encoded += "0"; // version string

        encoded.push_str(&format!("{:x}", self.wep_set));
        encoded.push_str(&format!("{:02x}", self.wep_id));

        for item in &[&self.head, &self.clothes, &self.shoes] {
            encoded.push_str(&item.encode());
        }

        encoded
    }
}

impl Default for Loadout {
    fn default() -> Self {
        Self {
            wep_set: 0,
            wep_id: 0,
            head: GearItem::default(),
            clothes: GearItem::default(),
            shoes: GearItem::default(),
        }
    }
}

#[derive(Debug)]
pub enum ParseFailure {
    InvalidHex,
    InvalidLoadout,
    InvalidGearItem,
}

impl std::fmt::Display for ParseFailure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::InvalidHex => "invalid hex value read",
                Self::InvalidLoadout => "loadout is incorrectly formed",
                Self::InvalidGearItem => "gear item is incorrectly formed",
            }
        )
    }
}

impl std::error::Error for ParseFailure {}

impl std::str::FromStr for Loadout {
    type Err = ParseFailure;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // VALIDATION
        //
        // validate various constraints with loadouts.
        if s.len() != 25 || !s.is_ascii() {
            return Err(ParseFailure::InvalidHex);
        }

        // The first character in a loadout.ink string is the version of the
        // string. According to
        // `https://github.com/selicia/splat2-calc/blob/master/app/encode.js`,
        // this is currently 0, and will be for likely the forseeable future.
        //
        // However, we'll still validate it for correctness's sake.
        let version = hex_to_bin(s.substring(0, 1))?;

        match u32::from_str_radix(&version, 2) {
            Ok(v) => {
                if v != 0 {
                    return Err(ParseFailure::InvalidLoadout);
                }
            }
            Err(_) => return Err(ParseFailure::InvalidLoadout),
        }

        // The next character is the loadout's weapon set.
        let wep_set = {
            let s = hex_to_bin(s.substring(1, 2))?;
            if let Ok(v) = u32::from_str_radix(&s, 2) {
                v
            } else {
                return Err(ParseFailure::InvalidLoadout);
            }
        };

        // The next two are the weapon's ID.
        let wep_id = {
            let s = hex_to_bin(s.substring(2, 4))?;
            if let Ok(v) = u32::from_str_radix(&s, 2) {
                v
            } else {
                return Err(ParseFailure::InvalidLoadout);
            }
        };

        // Finally, the rest of the characters can be split into 3 sets of 7
        // characters. Each set represents a gear item in the set, in the order:
        // headgear, clothing, and shoes.

        let head = s.substring(4, 11).parse()?;
        let clothes = s.substring(11, 18).parse()?;
        let shoes = s.substring(18, 25).parse()?;

        Ok(Loadout {
            wep_set,
            wep_id,
            head,
            clothes,
            shoes,
        })
    }
}

static mut HEX_BIN_MAP: Option<std::collections::HashMap<char, &'static str>> = None;
static HEX_BIN_MAP_INIT: std::sync::Once = std::sync::Once::new();

fn hex_to_bin(input: impl AsRef<str>) -> Result<String, ParseFailure> {
    let mut result = String::new();
    HEX_BIN_MAP_INIT.call_once(|| {
        let mut map = std::collections::HashMap::new();
        map.insert('0', "0000");
        map.insert('1', "0001");
        map.insert('2', "0010");
        map.insert('3', "0011");
        map.insert('4', "0100");
        map.insert('5', "0101");
        map.insert('6', "0110");
        map.insert('7', "0111");
        map.insert('8', "1000");
        map.insert('9', "1001");
        map.insert('a', "1010");
        map.insert('b', "1011");
        map.insert('c', "1100");
        map.insert('d', "1101");
        map.insert('e', "1110");
        map.insert('f', "1111");
        map.insert('A', "1010");
        map.insert('B', "1011");
        map.insert('C', "1100");
        map.insert('D', "1101");
        map.insert('E', "1110");
        map.insert('F', "1111");
        unsafe {
            HEX_BIN_MAP = Some(map);
        }
    });

    for ch in input.as_ref().chars() {
        unsafe {
            if let Some(bin) = HEX_BIN_MAP.as_ref().unwrap().get(&ch) {
                result += bin;
            } else {
                return Err(ParseFailure::InvalidHex);
            }
        }
    }

    Ok(result)
}

#[cfg(test)]
mod test {

    use crate::GearItem;
    use crate::Loadout;

    #[test]
    fn forward_back() {
        let test_str = "080311694ac62098ce6e214e5";
        let res = test_str.parse::<Loadout>().unwrap();

        assert_eq!(
            res,
            Loadout {
                wep_set: 8,
                wep_id: 3,
                head: GearItem {
                    gear_id: 17,
                    main: 13,
                    subs: [5, 5, 12],
                },
                clothes: GearItem {
                    gear_id: 98,
                    main: 1,
                    subs: [6, 6, 14],
                },
                shoes: GearItem {
                    gear_id: 110,
                    main: 4,
                    subs: [5, 7, 5],
                },
            }
        );

        let re_encode = res.encode();

        assert_eq!(re_encode, test_str);
    }

    #[test]
    fn identity() {
        let test_cases = [
            "000c495294a864a4a518c20e6",
            "05097d820e64c98000115298c",
            "000122294a736a31063ec39ce",
        ];

        for test in &test_cases {
            assert_eq!(test, &test.parse::<Loadout>().unwrap().encode());
        }
    }
}
