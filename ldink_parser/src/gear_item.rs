use substring::Substring;

#[derive(Debug, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde-derive", derive(serde::Serialize, serde::Deserialize))]
pub struct GearItem {
    pub gear_id: u16,
    pub main: u8,
    pub subs: [u8; 3],
}

impl GearItem {
    pub(crate) fn encode(&self) -> String {
        let mut encoded = String::new();

        encoded.push_str(&format!("{:05b}", self.gear_id));
        encoded.push_str(&format!("{:05b}", self.main));
        encoded.push_str(&format!("{:05b}", self.subs[0]));
        encoded.push_str(&format!("{:05b}", self.subs[1]));
        encoded.push_str(&format!("{:05b}", self.subs[2]));

        let to_int = u64::from_str_radix(&encoded, 2).unwrap();

        format!("{:x}", to_int)
    }
}

impl Default for GearItem {
    fn default() -> Self {
        Self {
            gear_id: 0,
            main: 0,
            subs: [0; 3],
        }
    }
}

impl std::str::FromStr for GearItem {
    type Err = crate::ParseFailure;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let gear_id = if let Ok(v) = u16::from_str_radix(s.substring(0, 2), 16) {
            v
        } else {
            return Err(crate::ParseFailure::InvalidGearItem);
        };

        let s = crate::hex_to_bin(s.substring(2, 7))?;

        let main = if let Ok(v) = u8::from_str_radix(s.substring(0, 5), 2) {
            v
        } else {
            return Err(crate::ParseFailure::InvalidGearItem);
        };

        let sub1 = if let Ok(v) = u8::from_str_radix(s.substring(5, 10), 2) {
            v
        } else {
            return Err(crate::ParseFailure::InvalidGearItem);
        };
        let sub2 = if let Ok(v) = u8::from_str_radix(s.substring(10, 15), 2) {
            v
        } else {
            return Err(crate::ParseFailure::InvalidGearItem);
        };
        let sub3 = if let Ok(v) = u8::from_str_radix(s.substring(15, 20), 2) {
            v
        } else {
            return Err(crate::ParseFailure::InvalidGearItem);
        };

        Ok(GearItem {
            gear_id,
            main,
            subs: [sub1, sub2, sub3],
        })
    }
}
