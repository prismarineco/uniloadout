use crate::models::player::{Player, PlayerEdit, PlayerID};
use actix_web::{web, HttpResponse, Result as ActixResult};

pub fn config(conf: &mut web::ServiceConfig) {
    conf.service(
        web::scope("/player")
            .service(new)
            .service(get)
            .service(edit)
            .service(delete),
    );
}

/// Required data to create a new player.
#[derive(Debug, serde::Deserialize)]
#[doc(hidden)]
struct NewPlayer {
    name: String,
}

#[actix_web::post("/new")]
async fn new(
    dat: web::Query<NewPlayer>,
    db: web::Data<sqlx::MySqlPool>,
) -> ActixResult<HttpResponse> {
    let mut np = Player::new(&dat.name);

    // if let Err(e) = np.update(&db).await {
    //     let resp = serde_json::json!({ "err": format!("{}", e) });

    //     HttpResponse::InternalServerError()
    //         .content_type("application/json")
    //         .body(resp)
    // } else {
    //     HttpResponse::Ok()
    //         .content_type("application/json")
    //         .json(&np)
    // }
    np.update(&db)
        .await
        .map_err::<actix_web::Error, _>(|x| crate::misc::Error(x).into())?;
    Ok(HttpResponse::Ok().json(&np))
}

#[actix_web::get("/{player_id}")]
async fn get(
    pid: web::Path<PlayerID>,
    db: web::Data<sqlx::MySqlPool>,
) -> ActixResult<HttpResponse> {
    let player_id = pid.into_inner();

    match Player::get(&db, player_id)
        .await
        .map_err::<actix_web::Error, _>(|x| crate::misc::Error(x).into())?
    {
        Some(p) => Ok(HttpResponse::Ok().json(&p)),
        None => Err(HttpResponse::NotFound().finish().into()),
    }
}

#[actix_web::patch("/{player_id}")]
async fn edit(
    pid: web::Path<PlayerID>,
    db: web::Data<sqlx::MySqlPool>,
    ed: web::Json<PlayerEdit>,
) -> ActixResult<HttpResponse> {
    let player_id = pid.into_inner();
    let ed = ed.into_inner();

    match Player::get(&db, player_id)
        .await
        .map_err::<actix_web::Error, _>(|x| crate::misc::Error(x).into())?
    {
        Some(mut p) => match p.edit(ed) {
            Ok(p) => {
                p.update(&db)
                    .await
                    .map_err::<actix_web::Error, _>(|x| crate::misc::Error(x).into())?;
                Ok(HttpResponse::NoContent().finish())
            }
            Err(e) => Err(HttpResponse::BadRequest()
                .json(&serde_json::json!({ "err": &format!("{}", e) }))
                .into()),
        },
        None => Err(HttpResponse::NotFound().finish().into()),
    }
}

#[actix_web::delete("/{player_id}")]
async fn delete(
    pid: web::Path<PlayerID>,
    db: web::Data<sqlx::MySqlPool>,
) -> ActixResult<HttpResponse> {
    let player_id = pid.into_inner();

    match Player::get(&db, player_id)
        .await
        .map_err::<actix_web::Error, _>(|x| crate::misc::Error(x).into())?
    {
        Some(p) => {
            p.delete(&db)
                .await
                .map_err::<actix_web::Error, _>(|x| crate::misc::Error(x).into())?;
            Ok(HttpResponse::NoContent().finish())
        }
        None => Err(HttpResponse::NotFound().finish().into()),
    }
}
