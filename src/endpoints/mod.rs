//! Various HTTP endpoints for the Universal Loadout System.

use actix_web::{web, HttpResponse, Responder};

mod player;

/// Add all endpoints of the API. This function should delegate to other
/// functions specific to their API groups (e.g. `endpoints::foo::config`,
/// `endpoints::bar::config`, etc.)
pub fn config(conf: &mut web::ServiceConfig) {
    conf.service(
        web::scope(&format!("/api/{}", env!("CARGO_PKG_VERSION")))
            .service(health)
            .configure(player::config),
    );
}

#[actix_web::get("/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/plain")
        .body("All clear!")
}
