use slog::Drain;
use slog_async::Async;
use slog_envlogger::LogBuilder;

#[allow(dead_code)]
static mut LOG_GUARD_SCOPE: Option<slog_scope::GlobalLoggerGuard> = None;

pub fn setup(conf: &crate::conf::Config) {
    let term_decorator = slog_term::TermDecorator::new().stderr().build();
    let term_drain = slog_term::FullFormat::new(term_decorator)
        .use_utc_timestamp()
        .build()
        .fuse();
    let term_drain = LogBuilder::new(term_drain)
        .parse(&conf.log.term_spec)
        .build();

    let term_drain = slog_async::Async::new(term_drain).build();

    let file = std::fs::OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(&conf.log.file_path)
        .expect("Failed to open log file");

    let file_dec = slog_term::PlainDecorator::new(file);
    let file_drain = slog_term::CompactFormat::new(file_dec)
        .use_utc_timestamp()
        .build()
        .fuse();

    let file_drain = LogBuilder::new(file_drain)
        .parse(&conf.log.file_spec)
        .build();

    let drain = slog::Duplicate::new(
        Async::new(file_drain.ignore_res()).build().fuse(),
        term_drain,
    );

    let root = slog::Logger::root(
        drain.fuse(),
        slog::o!("version" => env!("CARGO_PKG_VERSION")),
    );

    unsafe {
        LOG_GUARD_SCOPE = Some(slog_scope::set_global_logger(root));
    }
    slog_stdlog::init().expect("Failed to set logger");
}
