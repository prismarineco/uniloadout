use serde::{
    de::{self, Visitor},
    Deserialize, Deserializer, Serialize, Serializer,
};

use std::convert::TryFrom;
use std::fmt;

use serde_repr::{Deserialize_repr, Serialize_repr};

/// Splatoon 2 rank data.
#[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct Ranks {
    pub splat_zones: Rank,
    pub tower_control: Rank,
    pub rainmaker: Rank,
    pub clam_blitz: Rank,
    pub salmon_run: SalmonRank,
}

#[doc(hidden)]
#[derive(Debug, serde::Deserialize, Default)]
pub struct RankEdit {
    pub splat_zones: Option<Rank>,
    pub tower_control: Option<Rank>,
    pub rainmaker: Option<Rank>,
    pub clam_blitz: Option<Rank>,
    pub salmon_run: Option<SalmonRank>,
}

/// A strongly-typed representation of ranks in Splatoon 2.
///
/// Ranks are serialized as strings, with `Option<bool>` representing what part
/// of each rank a player is in. For example, `Rank::B(Some(true))` is
/// serialized to `B+`, `Rank::S(None)` is serialized to `S`, and
/// `Rank::C(Some(false))` is serialized to `C-`.
#[derive(Debug, Copy, Clone)]
pub enum Rank {
    C(Option<bool>),
    B(Option<bool>),
    A(Option<bool>),
    S(Option<u8>),
    X(u16),
}

#[derive(Debug, Copy, Clone, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum SalmonRank {
    Intern = 0,
    Apprentice = 1,
    PartTimer = 2,
    GoGetter = 3,
    Overachiever = 4,
    Profreshional = 5,
}

#[derive(Debug)]
pub struct InvalidRank;

impl TryFrom<u8> for SalmonRank {
    type Error = InvalidRank;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Intern),
            1 => Ok(Self::Apprentice),
            2 => Ok(Self::PartTimer),
            3 => Ok(Self::GoGetter),
            4 => Ok(Self::Overachiever),
            5 => Ok(Self::Profreshional),
            _ => Err(InvalidRank),
        }
    }
}

impl Default for Rank {
    fn default() -> Self {
        Self::C(Some(false))
    }
}

impl Default for SalmonRank {
    fn default() -> Self {
        Self::Intern
    }
}

impl fmt::Display for Rank {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Rank::C(bracket) => write!(
                f,
                "C{}",
                bracket.map(|x| if x { "+" } else { "-" }).unwrap_or("")
            ),
            Rank::B(bracket) => write!(
                f,
                "B{}",
                bracket.map(|x| if x { "+" } else { "-" }).unwrap_or("")
            ),
            Rank::A(bracket) => write!(
                f,
                "A{}",
                bracket.map(|x| if x { "+" } else { "-" }).unwrap_or("")
            ),
            Rank::S(bracket) => write!(
                f,
                "S{}",
                bracket.map(|x| format!("+{}", x)).unwrap_or_default()
            ),
            Rank::X(power) => write!(f, "X{}", power),
        }
    }
}

impl Serialize for Rank {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&format!("{}", self))
    }
}

impl From<(u8, Option<u16>)> for Rank {
    fn from((rnk, power): (u8, Option<u16>)) -> Self {
        match rnk {
            0 => Rank::C(Some(false)),
            1 => Rank::C(None),
            2 => Rank::C(Some(true)),
            3 => Rank::B(Some(false)),
            4 => Rank::B(None),
            5 => Rank::B(Some(true)),
            6 => Rank::A(Some(false)),
            7 => Rank::A(None),
            8 => Rank::A(Some(true)),
            9 => Rank::S(None),
            10 => Rank::S(Some(0)),
            11 => Rank::S(Some(1)),
            12 => Rank::S(Some(2)),
            13 => Rank::S(Some(3)),
            14 => Rank::S(Some(4)),
            15 => Rank::S(Some(5)),
            16 => Rank::S(Some(6)),
            17 => Rank::S(Some(7)),
            18 => Rank::S(Some(8)),
            19 => Rank::S(Some(9)),
            20..=u8::MAX => Rank::X(power.unwrap_or(2000)),
        }
    }
}

impl From<Rank> for (u8, Option<u16>) {
    fn from(rnk: Rank) -> Self {
        match rnk {
            Rank::C(Some(false)) => (0, None),
            Rank::C(None) => (1, None),
            Rank::C(Some(true)) => (2, None),
            Rank::B(Some(false)) => (3, None),
            Rank::B(None) => (4, None),
            Rank::B(Some(true)) => (5, None),
            Rank::A(Some(false)) => (6, None),
            Rank::A(None) => (7, None),
            Rank::A(Some(true)) => (8, None),
            Rank::S(None) => (9, None),
            Rank::S(Some(0)) => (10, None),
            Rank::S(Some(1)) => (11, None),
            Rank::S(Some(2)) => (12, None),
            Rank::S(Some(3)) => (13, None),
            Rank::S(Some(4)) => (14, None),
            Rank::S(Some(5)) => (15, None),
            Rank::S(Some(6)) => (16, None),
            Rank::S(Some(7)) => (17, None),
            Rank::S(Some(8)) => (18, None),
            Rank::S(Some(9)) => (19, None),
            Rank::S(Some(10..=u8::MAX)) => (20, Some(2000)),
            Rank::X(pow) => (20, Some(pow)),
        }
    }
}

struct RankVisitor;

impl<'de> Visitor<'de> for RankVisitor {
    type Value = Rank;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a valid rank")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        let mut v = v.chars();

        if let Some(ch) = v.next() {
            match ch {
                'C' => match v.next() {
                    None | Some(' ') => Ok(Rank::C(None)),
                    Some('+') => Ok(Rank::C(Some(true))),
                    Some('-') => Ok(Rank::C(Some(false))),
                    Some(c) => Err(de::Error::invalid_value(
                        de::Unexpected::Char(c),
                        &"either '+', '-', or nothing.",
                    )),
                },
                'B' => match v.next() {
                    None | Some(' ') => Ok(Rank::B(None)),
                    Some('+') => Ok(Rank::B(Some(true))),
                    Some('-') => Ok(Rank::B(Some(false))),
                    Some(c) => Err(de::Error::invalid_value(
                        de::Unexpected::Char(c),
                        &"either '+', '-', or nothing.",
                    )),
                },
                'A' => match v.next() {
                    None | Some(' ') => Ok(Rank::A(None)),
                    Some('+') => Ok(Rank::A(Some(true))),
                    Some('-') => Ok(Rank::A(Some(false))),
                    Some(c) => Err(de::Error::invalid_value(
                        de::Unexpected::Char(c),
                        &"either '+', '-', or nothing.",
                    )),
                },
                'S' => match v.next() {
                    None => Ok(Rank::S(None)),
                    Some('+') => match v.next() {
                        None => Ok(Rank::S(Some(0))),
                        Some(digit) if digit.is_ascii_digit() => Ok(Rank::S(Some(
                            (digit.to_string())
                                .parse::<u8>()
                                .expect("This message shouldn't be seen"),
                        ))),
                        Some(c) => Err(de::Error::invalid_value(
                            de::Unexpected::Char(c),
                            &"an ASCII digit (0-9)",
                        )),
                    },
                    Some(c) => Err(de::Error::invalid_value(
                        de::Unexpected::Char(c),
                        &"either '+', '-', or nothing.",
                    )),
                },
                'X' => {
                    let s = v.as_str();

                    if !s.chars().all(|x| x.is_ascii_digit()) {
                        Err(de::Error::invalid_value(
                            de::Unexpected::Str(&s),
                            &"a valid unsigned 16-bit value",
                        ))
                    } else if let Ok(v) = s.parse() {
                        Ok(Rank::X(v))
                    } else {
                        Err(de::Error::invalid_value(
                            de::Unexpected::Str(v.as_str()),
                            &"A valid unsigned 16-bit value",
                        ))
                    }
                }
                c => Err(de::Error::invalid_value(
                    de::Unexpected::Char(c),
                    &"either '+', '-', or nothing.",
                )),
            }
        } else {
            Err(de::Error::invalid_value(
                de::Unexpected::Other("empty string"),
                &"a valid rank",
            ))
        }
    }
}

impl<'de> Deserialize<'de> for Rank {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(RankVisitor)
    }
}
