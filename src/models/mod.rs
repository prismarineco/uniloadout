//! Various data models to be passed around, recieved, and processed by the API.

pub mod gamemode;
pub mod league;
pub mod player;
pub mod rank;
