use serde::{
    de::{self, Visitor},
    Deserialize, Deserializer, Serialize, Serializer,
};

#[derive(Debug)]
pub enum Gamemode {
    SplatZones,
    TowerControl,
    Rainmaker,
    ClamBlitz,
}

impl Serialize for Gamemode {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(match self {
            Self::SplatZones => "splat_zones",
            Self::TowerControl => "tower_control",
            Self::Rainmaker => "rainmaker",
            Self::ClamBlitz => "clam_blitz",
        })
    }
}

struct GamemodeVisitor;

impl<'de> Visitor<'de> for GamemodeVisitor {
    type Value = Gamemode;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a valid rank")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match v {
            "splat_zones" => Ok(Gamemode::SplatZones),
            "tower_control" => Ok(Gamemode::TowerControl),
            "rainmaker" => Ok(Gamemode::Rainmaker),
            "clam_blitz" => Ok(Gamemode::ClamBlitz),
            e => {
                Err(de::Error::invalid_value(
                    de::Unexpected::Str(e),
                    &"a valid string denoting a gamemode ('splat_zones', 'tower_control', 'rainmaker', 'clam_blitz')."
                ))
            }
        }
    }
}

impl<'de> Deserialize<'de> for Gamemode {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(GamemodeVisitor)
    }
}
