#![allow(dead_code)]

use crate::models::{gamemode::Gamemode, player::PlayerID};
use chrono::{DateTime, Utc};
use serde_repr::*;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
#[repr(transparent)]
#[serde(transparent)]
pub struct LeagueRecID(uuid::Uuid);

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum LeagueRecType {
    Pair = 0,
    Team = 1,
}

pub struct LeagueRecord {
    id: LeagueRecID,
    time: DateTime<Utc>,
    power: u16,
    mode: Gamemode,
    rec_type: LeagueRecType,
    players: [Option<PlayerID>; 4],
}

impl LeagueRecord {
    fn new(
        time: DateTime<Utc>,
        power: u16,
        mode: Gamemode,
        rec_type: LeagueRecType,
        players: [Option<PlayerID>; 4],
    ) -> Self {
        Self {
            id: LeagueRecID(uuid::Uuid::new_v4()),
            time,
            power,
            mode,
            rec_type,
            players,
        }
    }

    fn delete(self) {
        todo!()
    }
}
