use crate::models::rank::{RankEdit, Ranks, SalmonRank};
use ldink_parser::Loadout;
use std::collections::HashSet;
use std::convert::TryFrom;

#[derive(Debug, serde::Serialize, serde::Deserialize, sqlx::Type)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct PlayerID(pub uuid::Uuid);

/// Splatoon 2 Player data, along with API-specific datapoints.
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Player {
    /// A player's in-game name. 10 character maximum.
    name: String,

    /// A player's Uniloadout ID. Used to uniquely identify them in the API and read data related to them.
    id: PlayerID,

    /// A player's level. This is represented as a single unsigned integer, and
    /// so will require translation between the in-game listing and internal
    /// representation. A simple equation for this would be `f(p, l) = 98p + l`,
    /// where `p` represents the number of times a player has prestiged, and `l`
    /// represents a player's in-game displayed level.
    level: u16,

    /// A player's various rankings.
    ranks: Ranks,

    /// A player's loadouts.
    loadouts: HashSet<Loadout>,
}

#[doc(hidden)]
#[derive(Debug, serde::Deserialize, Default)]
pub struct PlayerEdit {
    name: Option<String>,
    level: Option<u16>,
    ranks: Option<RankEdit>,
    loadout_add: Option<HashSet<String>>,
    loadout_remove: Option<HashSet<String>>,
}

macro_rules! edit_field {
    ($field:ident, $obj:ident, $other:ident) => {
        if let Some(__val__) = $other.$field {
            $obj.$field = __val__;
        }
    };
}

impl Player {
    /// Generate a player with a brand new Uniloadout ID.
    pub fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            id: PlayerID(uuid::Uuid::new_v4()),
            level: 1,
            ranks: Ranks::default(),
            loadouts: HashSet::new(),
        }
    }

    /// Acquire all of a player's data from the database.
    pub async fn get(db: &sqlx::MySqlPool, id: PlayerID) -> Result<Option<Self>, sqlx::Error> {
        // First, the basic player data.
        let player = sqlx::query!(
            "SELECT id,
                    name,
                    level,
                    sz_rank,
                    sz_power,
                    tc_rank,
                    tc_power,
                    rm_rank,
                    rm_power,
                    cb_rank,
                    cb_power,
                    salmon_run
            FROM players
            INNER JOIN ranks
                USING (id)
            WHERE id = ?",
            id
        )
        .fetch_optional(db)
        .await?;

        if let Some(player) = player {
            let loadouts = sqlx::query!("SELECT ld FROM loadouts WHERE id = ?", id)
                .fetch_all(db)
                .await?
                .iter()
                // TODO: Remember to validate loadouts as they're inserted!
                .map(|x| {
                    x.ld.parse::<Loadout>()
                        .expect("Invalid loadout in the database")
                })
                .collect();

            Ok(Some(Self {
                id,
                name: player.name,
                level: player.level,
                ranks: Ranks {
                    splat_zones: (player.sz_rank, player.sz_power).into(),
                    tower_control: (player.tc_rank, player.tc_power).into(),
                    rainmaker: (player.rm_rank, player.rm_power).into(),
                    clam_blitz: (player.cb_rank, player.cb_power).into(),
                    salmon_run: SalmonRank::try_from(player.salmon_run)
                        .expect("Invalid rank(sr) in the database"),
                },
                loadouts,
            }))
        } else {
            Ok(None)
        }
    }

    pub fn edit(&mut self, ed: PlayerEdit) -> Result<&mut Self, ldink_parser::ParseFailure> {
        edit_field!(name, self, ed);
        edit_field!(level, self, ed);
        if let Some(ed_rnk) = ed.ranks {
            let mut rnk = &mut self.ranks;
            edit_field!(splat_zones, rnk, ed_rnk);
            edit_field!(tower_control, rnk, ed_rnk);
            edit_field!(rainmaker, rnk, ed_rnk);
            edit_field!(clam_blitz, rnk, ed_rnk);
            edit_field!(salmon_run, rnk, ed_rnk);
        }
        if let Some(ed_loadout_add) = ed.loadout_add {
            for ld in &ed_loadout_add {
                self.loadouts.insert(ld.parse()?);
            }
        }
        if let Some(ed_loadout_rm) = ed.loadout_remove {
            for ld in &ed_loadout_rm {
                self.loadouts.remove(&ld.parse::<Loadout>()?);
            }
        }
        Ok(self)
    }

    /// Update a player's data within the database. Can also be used for insertions.
    pub async fn update(&mut self, db: &sqlx::MySqlPool) -> Result<(), sqlx::Error> {
        // Start a transaction for the process. Async functions aren't atomic, but transactions are!
        let mut transact = db.begin().await?;

        sqlx::query!(
            "
            REPLACE INTO players(id, name, level) VALUES (?,?,?);
            ",
            self.id,
            self.name,
            self.level
        )
        .execute(&mut transact)
        .await?;

        let (sz_rank, sz_power) = self.ranks.splat_zones.into();
        let (tc_rank, tc_power) = self.ranks.tower_control.into();
        let (rm_rank, rm_power) = self.ranks.rainmaker.into();
        let (cb_rank, cb_power) = self.ranks.clam_blitz.into();

        sqlx::query!(
            "
                REPLACE INTO ranks(id, sz_rank, sz_power, tc_rank, tc_power, rm_rank, rm_power, cb_rank, cb_power, salmon_run) VALUES (?,?,?,?,?,?,?,?,?,?);
            ", self.id,
                sz_rank,
            sz_power,
            tc_rank,
            tc_power,
            rm_rank,
            rm_power,
            cb_rank,
            cb_power,
                self.ranks.salmon_run as u8,
        ).execute(&mut transact)
            .await?;

        for ld in &self.loadouts {
            sqlx::query!(
                "
                    INSERT INTO loadouts(id,ld) VALUES (?,?) ON DUPLICATE KEY UPDATE id=id;
                ",
                self.id,
                ld.encode()
            )
            .execute(&mut transact)
            .await?;
        }

        transact.commit().await?;

        Ok(())
    }

    /// Delete a player's data within the API.
    pub async fn delete(self, db: &sqlx::MySqlPool) -> Result<(), sqlx::Error> {
        let mut transact = db.begin().await?;

        // Since `ranks` and `loadouts` have their entries marked with `ON
        // DELETE CASCADE`, all we need to do is remove the player entry and the
        // rest follow. Simple!
        sqlx::query!("DELETE FROM players WHERE id = ?", self.id)
            .execute(&mut transact)
            .await?;

        transact.commit().await?;

        Ok(())
    }
}
