#![deny(clippy::unwrap_used)]

extern crate actix_web;
extern crate blake3;
extern crate chrono;
extern crate dotenv;
extern crate ldink_parser;
extern crate rand;
extern crate serde;
extern crate serde_json;
extern crate serde_repr;
extern crate slog;
extern crate slog_async;
extern crate slog_envlogger;
extern crate slog_scope;
extern crate slog_stdlog;
extern crate slog_term;
extern crate sqlx;
extern crate uuid;

use actix_web::{middleware, App, HttpServer as Server};

/// HTTP Endpoints of the API.
mod endpoints;

/// Data representations of the API.
mod models;

/// Configuration parsing and organization.
mod conf;
use conf::Config;

/// Logging setup and control.
mod logging;

/// Miscelaneous items and implementations.
mod misc;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Get the API's config
    let conf = Config::read().expect("Invalid config");

    // setup logging
    crate::logging::setup(&conf);

    // Connect to the db
    let db = sqlx::MySqlPool::connect(&conf.database.url)
        .await
        .expect("Failed to connect to the database");

    // run the migrations
    sqlx::migrate!("./migrations")
        .run(&db)
        .await
        .expect("Failed to synchronize database schema");

    let server = Server::new(move || {
        App::new()
            .data(db.clone())
            .wrap(middleware::Logger::default())
            .wrap(middleware::DefaultHeaders::new().header("X-Version", env!("CARGO_PKG_VERSION")))
            .configure(endpoints::config)
    });

    server.bind("0.0.0.0:5000")?.run().await
}
