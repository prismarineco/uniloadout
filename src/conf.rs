use std::env;

#[derive(Debug)]
pub struct Config {
    pub database: Database,
    pub log: Logging,
}

#[derive(Debug)]
pub struct Database {
    pub url: String,
}

#[derive(Debug)]
pub struct Logging {
    pub term_spec: String,
    pub file_spec: String,
    pub file_path: String,
}

impl Config {
    pub fn read() -> Result<Config, Box<dyn std::error::Error + Send + Sync + 'static>> {
        dotenv::dotenv().ok();

        let database = Database {
            url: env::var("DATABASE_URL")?,
        };

        let log = Logging {
            term_spec: env::var("TERMINAL_RUST_LOG")?,
            file_spec: env::var("FILE_RUST_LOG")?,
            file_path: env::var("FILE_LOG_PATH")
                .or_else::<std::convert::Infallible, _>(|_| Ok("uniloadout.log".into()))
                .expect(""), // this is just to shut clippy up. Yes, using
                             // #[allow] would be more correct, but this is
                             // easier.
        };

        Ok(Config { database, log })
    }
}
